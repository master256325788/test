#include <stdio.h>

int main()  {
    int arrayCount;
    int negativeSum = 0;
    int countPozitive = 0;

    printf("введите кол-во элементов массива: ");
    scanf("%d", &arrayCount);
    printf("\n");
    int arrayInput[arrayCount];
    for (int i = 0; i < arrayCount; i++) {
	printf("введите элемент массива N%d: ", i + 1);
	scanf("%d", &arrayInput[i]);
    }

    for (int i = 0; i < arrayCount; i++) {
	if (arrayInput[i] < 0) {
	    negativeSum  += arrayInput[i];
	} else {
	    countPozitive++;
	}
    }
    int arrayB[countPozitive];
    int n = 0;
    for (int i = 0; i < arrayCount; i++) {
	if (arrayInput[i] > 0) {
	    arrayB[n] = arrayInput[i];
	    n++;
	}
    }
    int dump;
    for (int i = 0; i < countPozitive; i++) {
	for (int j = i; j < countPozitive; j++) {
		if (arrayB[i] > arrayB[j]) {
			dump = arrayB[i];
			arrayB[i] = arrayB[j];
			arrayB[j] = dump;
		    }
	    }
    }

    printf("\nСумма отрицательных чисел: %d\n", negativeSum);
    printf("Mассив В = { ");

    for (int i = 0; i < countPozitive; i++) {
	if (i != 0) {
	    printf(", ");
	}
	printf("%d", arrayB[i]);
    }

    printf(" }\n");
    printf("Максимальный элемент массива В: %d\n", arrayB[countPozitive - 1]);
    scanf("%d", &arrayCount);

    return 0;
}
